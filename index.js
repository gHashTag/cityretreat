import React from 'react'
import { AppRegistry, UIManager } from 'react-native'
import { ApolloProvider } from 'react-apollo'
import { store, client } from './src/store'
import AppNavigation from './src/navigations'

if (UIManager.setLayoutAnimationEnabledExperimental) {
  UIManager.setLayoutAnimationEnabledExperimental(true)
}

export default class App extends React.Component {
  render() {
    return (
      <ApolloProvider store={store} client={client}>
        <AppNavigation />
      </ApolloProvider>
    )
  }
}

AppRegistry.registerComponent('cityretreat', () => App)

import React, { Component } from 'react'
import {
  View,
  TextInput,
  ScrollView
} from 'react-native'
import { Fumi } from 'react-native-textinput-effects'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FitImage from 'react-native-fit-image'
import { 
  Header
} from '../common'

const colortheme = '#00FFC5'

class Detail extends Component {
  static navigationOptions = ({ navigation }) => ({
    header:  
      <Header 
        title={`${navigation.state.params.subtitle} ${'\u20bd'}`}
        leftButton
        leftIcon='ios-arrow-back'
        colorLeft={colortheme}
        rightButton
        rightIcon='md-create'
        colorRight='#242323'
        navigation={navigation} 
      />
  })

  render() {
    const { img, info, title, subtitle, limit, amount } = this.props.navigation.state.params
    const { container, inputContainer, inputStyle, labelStyle } = styles

    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={container}>
          <FitImage source={{ uri: img }} />
          <View style={{ paddingTop: 20, paddingBottom: 200 }}>
            <Fumi
              label={'Тип абонемента'}
              iconClass={FontAwesome}
              iconName={'handshake-o'}
              iconColor={colortheme}
              iconSize={21}
              autoCorrect={false}
              returnKeyType='done'
              autoCapitalize='words'
              editable = {false}
              underlineColorAndroid='transparent'
              value={title}
              inputStyle={inputStyle}
              labelStyle={labelStyle}
              style={{ backgroundColor: 'transparent' }}
            />
            <Fumi
              label={'Стоимость'}
              iconClass={FontAwesome}
              iconName={'money'}
              iconColor={colortheme}
              iconSize={21}
              returnKeyType='done'
              underlineColorAndroid='transparent'
              keyboardType='numeric'
              editable={false}
              value={`${subtitle} ${'\u20bd'}`}
              inputStyle={inputStyle}
              labelStyle={labelStyle}
              style={{ backgroundColor: 'transparent' }}
            />
            <Fumi
              label={'Срок действия'}
              iconClass={FontAwesome}
              iconName={'calendar'}
              iconColor={colortheme}
              iconSize={21}
              autoCorrect={false}
              returnKeyType='done'
              autoCapitalize='words'
              keyboardType='numeric'
              editable={false}
              underlineColorAndroid='transparent'
              value={`Лимит: ${limit} - количество месяцев`}
              inputStyle={inputStyle}
              labelStyle={labelStyle}
              style={{ backgroundColor: 'transparent' }}
            />
            <Fumi
              label={'Количество занятий'}
              iconClass={FontAwesome}
              iconName={'check'}
              iconColor={colortheme}
              iconSize={21}
              autoCorrect={false}
              returnKeyType='done'
              autoCapitalize='words'
              editable={false}
              underlineColorAndroid='transparent'
              keyboardType='numeric'
              value={`Занятий: ${amount}`}
              inputStyle={inputStyle}
              labelStyle={labelStyle}
              style={{ backgroundColor: 'transparent' }}
            />
            <TextInput
              style={inputContainer}
              placeholder="Подробности" 
              editable={false}
              multiline
              value={info}
            />
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: 'rgba(36, 36, 35, 0.4)',
    position: 'relative'
  },
  inputContainer: {
    fontSize: 19,
    width: '100%',
    color: '#DBDCDC', 
    fontFamily: 'AppleSDGothicNeo-Light', 
    backgroundColor: 'transparent',
    padding: 15,
    fontWeight: '400' 
  },
  inputStyle: { 
    color: '#DBDCDC', 
    fontFamily: 'AppleSDGothicNeo-Light', 
    backgroundColor: 'transparent',
    paddingBottom: 23,
    fontWeight: '400' 
  }
}

export default Detail

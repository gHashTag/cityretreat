import React, { Component } from 'react'
import {
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity
} from 'react-native'
import _ from 'lodash'
import Picker from 'react-native-wheel-picker'
import { withApollo, compose, graphql } from 'react-apollo'
import { connect } from 'react-redux'
import { createFilter } from 'react-native-search-filter'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Header, Loading, Separator } from '../common'
import * as actions from '../actions'

import GET_STUDIO_PRICES_QUERY from '../graphql/queries/price/getPrices'
import ADDED_SUBSCRIPTION from '../graphql/subscriptions/price/priceAdded'
import UPDATED_SUBSCRIPTION from '../graphql/subscriptions/price/priceUpdated'
import DELETED_SUBSCRIPTION from '../graphql/subscriptions/price/priceDeleted'
import { constants } from '../constants'

class Main extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: 
      <Header 
        title='Стоимость'
        leftButton
        leftIcon='md-add'
        colorLeft={constants.PRIMARY}
        rightButton
        rightIcon='ios-contact-outline'
        colorRight={constants.SECONDARY}
        navigation={navigation} 
        screen='User'
      />
  })

  constructor(props) {
    super(props)
    this.state = {
      searchTerm: '',
      keyToFilters: ['title'],
      selectedItem: '',
      selectedValue: '' 
    }
  }

  componentWillMount() {
    this.props.data.subscribeToMore({
      document: ADDED_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) {
          return prev
        }

        const priceItem = subscriptionData.data.priceAdded
        const shouldIgnore = (item) => {
          return ((priceItem.studio._id !== item.studio._id) || (priceItem._id === item._id))
        }

        if (prev.getPrices.some(shouldIgnore)) {
          console.log('garbage')
        } else {
          return { 
            ...prev,
            getPrices: [{ ...priceItem }, ...prev.getPrices] }
        }
        return prev 
      }
    })

    this.props.data.subscribeToMore({
      document: UPDATED_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) {
          return prev
        }

        const updateItem = subscriptionData.data.priceUpdated
        const priceVar = prev.getPrices.findIndex(item => (item._id === updateItem._id))
        const priceGetStudioPrices = [...prev.getPrices]
        priceGetStudioPrices[priceVar] = updateItem

        if (prev.getPrices.find(t => (t._id === updateItem._id))) {
          return {
            ...prev,
            getPrices: priceGetStudioPrices 
          }
        }
        return prev
      }
    })

    this.props.data.subscribeToMore({
      document: DELETED_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) {
          return prev
        }

        const deleted = subscriptionData.data.priceDeleted
        if (prev.getPrices.find(t => t._id === deleted._id)) {
          return {
            ...prev,
            getPrices: prev.getPrices.filter(t => t._id !== deleted._id)
          }
        }
        return prev
      }
    })
  }

  async componentDidMount() {
    const { data: { getPrices } } = await this.props.client.query({ query: GET_STUDIO_PRICES_QUERY })
    this.props.getDataPrices(getPrices)
  }

  onPickerSelect(index) {
    const { dataArray } = this.props
    const uniqueData = dataArray ? _.uniqBy(dataArray, 'title') : [] 
    this.setState({ 
      searchTerm: index !== uniqueData.length ? uniqueData[index].title : '',
      selectedItem: index
    })
  }

  render() {
    const { data: { getPrices, loading } } = this.props
    const { searchTerm, keyToFilters } = this.state
    const filteredData = getPrices ? getPrices.filter(createFilter(searchTerm, keyToFilters)) : []

    const uniqueData = getPrices ? _.uniqBy(getPrices, 'title') : [] 

    const { container, subContainer, thumbImg, titleContainer, titleContainer2, h1, chevron, itemStyle, picker } = styles

    if (loading) { return <Loading /> }

    return (
      <View style={container}>
        <ScrollView>
          <Picker
            style={picker}
            itemStyle={itemStyle}
            selectedValue={this.state.selectedItem}
            onValueChange={(index) => { this.onPickerSelect(index) } }
          >
            <Picker.Item label='Все' value={uniqueData.length} />
            {uniqueData.map((value, i) => (
              <Picker.Item 
                key={value._id} 
                value={i} 
                label={value.title} 
              />
            )
            )}
          </Picker>
          {filteredData.map(item => {
            return (
              <TouchableOpacity onPress={ () => this.props.navigation.navigate('Detail', (item)) } key={item._id} >
                <Separator />
                <View style={subContainer}>
                  <View style={titleContainer}>
                    <Image style={thumbImg} source={{ uri: item.img }} />
                    <View style={titleContainer2}>
                      <Text style={h1} numberOfLines={1} ellipsizeMode='tail'>{item.subtitle} {'\u20bd'}</Text>
                    </View>
                  </View>
                  <Icon style={chevron} name="chevron-right" size={50} color="#DBD7D2" />
                </View>
              </TouchableOpacity>
            )
          })}
        </ScrollView>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    marginTop: -50
  },
  subContainer: {
    flex: 1,
    backgroundColor: constants.WHITE,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5
  },
  titleContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    flexDirection: 'row'
  },
  thumbImg: {
    width: 100,
    height: 100
  },
  titleContainer2: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  h1: {
    backgroundColor: 'transparent',
    flex: -1,
    alignSelf: 'flex-start',
    justifyContent: 'flex-start',
    fontFamily: constants.FONT,
    paddingLeft: 10,
    fontWeight: '400',
    color: constants.FONTCOLOR,
    fontSize: 17
  },
  chevron: {
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
    alignSelf: 'center'
  },
  picker: {
    backgroundColor: constants.WHITE,
    height: 190,
    width: '100%'
  },
  itemStyle: {
    fontFamily: constants.FONT,
    backgroundColor: 'transparent',
    fontWeight: '700',
    color: constants.FONTCOLOR,
    fontSize: 20
  }
}

const mapStateToProps = (state) => {
  const { dataArray, loading } = state.prices
  return { dataArray, loading }
}

export default withApollo(compose(
  connect(mapStateToProps, actions),
  graphql(GET_STUDIO_PRICES_QUERY)
)(Main))

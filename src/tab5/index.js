import { StackNavigator } from 'react-navigation'
import Main from './Main'
import Detail from './Detail'
import { User } from '../common'

export default StackNavigator({
  Main: { screen: Main },
  Detail: { screen: Detail },
  User: { screen: User }
}, {
  headerMode: 'none'
})

import { combineReducers } from 'redux'
import nav from './navigation'
import news from './new'
import masters from './master'
import services from './service'
import prices from './price'

export default client => combineReducers({
  apollo: client.reducer(),
  news,
  masters,
  services,
  prices,
  nav
})


const initialState = {
  loading: false,
  dataArray: []
}

export default (state = initialState, action) => {
  //console.log('action', action)
  switch (action.type) {
  case 'GET_DATA_PRICES':
    return {
      ...state,
      dataArray: action.payload,
      loading: true
    }
  case 'GET_DATA_PRICES_FAIL':
    return {
      ...state
    }
  default:
    return state
  }
}

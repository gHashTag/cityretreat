const initialState = {
  loading: false,
  dataArray: []
}

export default (state = initialState, action) => {
  //console.log('action', action)
  switch (action.type) {
  case 'GET_DATA_NEWS':
    return {
      ...state,
      dataArray: action.payload,
      loading: true
    }
  case 'GET_DATA_NEWS_FAIL':
    return {
      ...state
    }
  default:
    return state
  }
}

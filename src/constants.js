export const constants = {
  PRIMARY: '#2A2525',
  SECONDARY: '#BFB08B',
  THIRD: '#323437',
  BLACK: '#242323',
  WHITE: '#FFFFFF',
  LIGHT_GRAY: '#7F7D7A',
  FONT: 'AppleSDGothicNeo-Light',
  FONTCOLOR: '#474747' 
}

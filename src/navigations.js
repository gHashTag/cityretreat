import React, { Component } from 'react'
import { Platform } from 'react-native'
import { connect } from 'react-redux'
import { addNavigationHelpers, StackNavigator, TabNavigator } from 'react-navigation'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { constants } from './constants'

import Tab1 from './tab1'
import Tab2 from './tab2'
import Tab3 from './tab3'
import Tab4 from './tab4'
import Tab5 from './tab5'
import Tab6 from './tab6'
//import { User } from './common'

const Tabs = TabNavigator({
  Tab1: {
    screen: Tab1,
    navigationOptions: { 
      tabBarLabel: 'Главная',
      tabBarIcon: ({ tintColor, focused }) => (
        <Ionicons
          name={focused ? 'ios-home' : 'ios-home-outline'}
          size={26}
          style={{ color: tintColor }}
        />
      )
    }
  },
  Tab2: {
    screen: Tab2,
    navigationOptions: { 
      tabBarLabel: 'Услуги',
      tabBarIcon: ({ tintColor, focused }) => (
        <Ionicons
          name={focused ? 'ios-star' : 'ios-star-outline'}
          size={26}
          style={{ color: tintColor }}
        />
      )
    }
  },
  Tab3: {
    screen: Tab3,
    navigationOptions: { 
      tabBarLabel: 'Лица',
      tabBarIcon: ({ tintColor, focused }) => (
        <Ionicons
          name={focused ? 'ios-school' : 'ios-school-outline'}
          size={26}
          style={{ color: tintColor }}
        />
      )
    }
  },
  Tab4: {
    screen: Tab4,
    navigationOptions: { 
      tabBarLabel: 'Расписание',
      tabBarIcon: ({ tintColor, focused }) => (
        <Ionicons
          name={focused ? 'ios-calendar' : 'ios-calendar-outline'}
          size={26}
          style={{ color: tintColor }}
        />
      )
    }
  },
  Tab5: {
    screen: Tab5,
    navigationOptions: { 
      tabBarLabel: 'Стоимость',
      tabBarIcon: ({ tintColor, focused }) => (
        <Ionicons
          name={focused ? 'ios-card' : 'ios-card-outline'}
          size={26}
          style={{ color: tintColor }}
        />
      )
    }
  },
  Tab6: {
    screen: Tab6,
    navigationOptions: () => ({
      tabBarLabel: 'Контакт',
      headerMode: 'screen',
      tabBarIcon: ({ tintColor, focused }) => (
        <Ionicons name={focused ? 'ios-call' : 'ios-call-outline'}
          size={26}
          style={{ color: tintColor }}
        />
      )
    })
  }
},
{
  tabBarOptions: {
    swipeEnabled: false,
    animationEnabled: false,
    upperCaseLabel: false,
    showIcon: true,
    showLabel: true,
    activeTintColor: constants.PRIMARY,
    inactiveTintColor: 'gray',
    renderIndicator: () => null,
    labelStyle: {
      fontFamily: constants.FONT,
      fontSize: Platform.OS === 'ios' ? 10 : 11, 
      width: 60, 
      color: constants.FONTCOLOR
    },
    style: {
      backgroundColor: '#fff',
      height: Platform.OS === 'ios' ? 53 : 65, 
      paddingVertical: 5,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.3,
      elevation: 4
    }
  },
  tabBarPosition: 'bottom',
  initialRouteName: 'Tab1'
})

const AppMainNav = StackNavigator({
  Home: { screen: Tabs }
})

class AppNavigation extends Component {
  render() {
    const nav = addNavigationHelpers({
      dispatch: this.props.dispatch,
      state: this.props.nav 
    })
    return <AppMainNav navigation={nav} /> 
  }
}

export default connect(state => ({
  nav: state.nav
}))(AppNavigation)

export const router = AppMainNav.router

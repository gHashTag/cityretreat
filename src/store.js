/* eslint-disable no-param-reassign */

import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import ApolloClient, { createNetworkInterface } from 'apollo-client'
import thunk from 'redux-thunk'
import { SubscriptionClient, addGraphQLSubscriptions } from 'subscriptions-transport-ws'

import reducers from './reducers'

const networkInterface = createNetworkInterface({
  uri: 'https://yogi-999.herokuapp.com/graphql'
})

const wsClient = new SubscriptionClient('ws://yogi-999.herokuapp.com/subscriptions', {
  reconnect: true,
  connectionParams: {}
})

const networkInterfaceWithSubs = addGraphQLSubscriptions(
  networkInterface,
  wsClient
)

export const client = new ApolloClient({
  networkInterface: networkInterfaceWithSubs
})

const middlewares = [client.middleware(), thunk]

export const store = createStore(
  reducers(client),
  undefined,
  composeWithDevTools(applyMiddleware(...middlewares)),
)

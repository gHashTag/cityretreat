import gql from 'graphql-tag'

export default gql`
  subscription {
    newAdded {
      _id
      title 
      subtitle 
      img
      info
      studio {
        _id
      }
    }
  }
`

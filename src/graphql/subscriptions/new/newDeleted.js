import gql from 'graphql-tag'

export default gql`
  subscription {
    newDeleted {
      _id
      title 
      subtitle 
      img
      info
      studio {
        _id
      }
    }
  }
`

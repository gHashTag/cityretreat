import gql from 'graphql-tag'

export default gql`
  subscription {
    priceAdded {
      _id
      title 
      subtitle 
      img
      info
      limit
      amount
      studio {
        _id
      }
    }
  }
`

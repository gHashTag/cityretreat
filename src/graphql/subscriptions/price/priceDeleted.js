import gql from 'graphql-tag'

export default gql`
  subscription {
    priceDeleted {
      _id
      title 
      subtitle 
      img
      info
      limit
      amount
      studio {
        _id
      }
    }
  }
`

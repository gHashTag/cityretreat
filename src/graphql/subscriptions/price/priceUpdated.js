import gql from 'graphql-tag'

export default gql`
  subscription {
    priceUpdated {
      _id
      title 
      subtitle 
      img
      info
      limit
      amount
      studio {
        _id
      }
    }
  }
`

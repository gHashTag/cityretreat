import gql from 'graphql-tag'

export default gql`
  subscription {
    serviceUpdated {
      _id
      title 
      subtitle 
      img
      info
      studio {
        _id
      }
    }
  }
`

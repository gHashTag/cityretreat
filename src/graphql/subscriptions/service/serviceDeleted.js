import gql from 'graphql-tag'

export default gql`
  subscription {
    serviceDeleted {
      _id
      title 
      subtitle 
      img
      info
      studio {
        _id
      }
    }
  }
`

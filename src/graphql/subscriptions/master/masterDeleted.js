import gql from 'graphql-tag'

export default gql`
  subscription {
    masterDeleted {
      _id
      title 
      subtitle 
      img
      info
      studio {
        _id
      }
    }
  }
`

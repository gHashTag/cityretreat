import gql from 'graphql-tag'

export default gql`
{
  getMasters(_id: "5a34e81c24c3f51400779e61") {
    _id
    title 
    subtitle 
    img
    info
    studio {
      _id
    }
  }
}
`

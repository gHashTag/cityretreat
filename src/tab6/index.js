import { StackNavigator } from 'react-navigation'
import Contact from './contact'
import { User } from '../common'

export default StackNavigator({
  Main: { screen: Contact },
  User: { screen: User }
}, {
  headerMode: 'none'
})

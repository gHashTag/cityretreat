import React, { Component } from 'react'
import {
  Animated,
  Easing,
  Image,
  Platform,
  StyleSheet,
  View,
  Dimensions
} from 'react-native'
import MapView from 'react-native-maps'

const { width, height } = Dimensions.get('window')

export default class Contact extends Component {
  constructor() {
    super()
    this.RotateValueHolder = new Animated.Value(0)
  }

  componentDidMount() {
    this.StartImageRotateFunction()
  }

  StartImageRotateFunction() {
    this.RotateValueHolder.setValue(0)
    Animated.timing(
      this.RotateValueHolder,
      {
        toValue: 1,
        duration: 3000,
        easing: Easing.linear
      }
    ).start(() => this.StartImageRotateFunction())
  }

  render() {
    const RotateData = this.RotateValueHolder.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })

    return (
      <View>
        <MapView
          style={styles.map}
          region={{
            latitude: 55.75810856895293,
            longitude: 37.555804499999965,
            latitudeDelta: 0.002,
            longitudeDelta: 0.002
          }}
        >
          <MapView.Marker
            coordinate={{ 
              latitude: 55.75770256895293,
              longitude: 37.556004499999965
            }}
            title='Эра Водолея'
            description='йога клуб'
          >
            <View>
              <Animated.Image
                style={{
                  width: 60,
                  height: 60,
                  transform: [{ rotate: RotateData }] }}
                source={require('./logo.png')}
              />
            </View>
          </MapView.Marker>

          <MapView.Marker
            coordinate={{ latitude: 55.77770822, longitude: 37.58618642 }}
            title='Метро Белорусская'
            description='выход на улицу Лесная'
          >
            <View>
              <Image style={{ width: 50, height: 60 }}
                source={require('./metro.png')}
              />
            </View>
          </MapView.Marker>

          <MapView.Polyline
            coordinates={[
              { latitude: 55.77763522, longitude: 37.58636642 },
              { latitude: 55.77632999, longitude: 37.58854 },
              { latitude: 55.77675698, longitude: 37.58919595 }
            ]}
            strokeColor='red'
            strokeWidth={3}
            lineDashPattern={[5, 2, 3, 2]}
          />
        </MapView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 20 : 0
  },
  infoBox: {
    width,
    paddingHorizontal: 15,
    marginBottom: 15
  },
  headTxt: {
    fontWeight: '700',
    marginBottom: 7
  },
  infoTxt: {
    fontWeight: '300',
    fontSize: 16
  },
  map: {
    width,
    height: height / 2
  }
})

'use strict'
import React, { Component } from 'react'

import {
  StyleSheet,
  Text,
  View,
  Linking,
  Dimensions,
  ScrollView
} from 'react-native'
import Communications from 'react-native-communications'
import IconS from 'react-native-vector-icons/SimpleLineIcons'
import { Header } from '../common'
import Map from './map' // eslint-disable-line
import { constants } from '../constants'

const win = Dimensions.get('window')

export default class Contact extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: 
      <Header 
        title='Кoнтакты'
        leftButton
        leftIcon='md-add'
        colorLeft={constants.PRIMARY}
        rightButton
        rightIcon='ios-contact-outline'
        colorRight={constants.SECONDARY}
        navigation={navigation} 
        screen='User'
      />
  })

  render() {
    const { container, icon, h1, a, b } = styles

    return (
      <View style={styles.container}>
        <ScrollView style={container}>
          <View style={{ paddingTop: 30, flexDirection: 'row' }}>
            <Text style={h1}>ТЕЛЕФОН</Text>
            <IconS 
              onPress={() => Communications.phonecall('+74992520132', true)}
              style={icon} name='phone' size={30} color={constants.SECONDARY} />
          </View>
          <Text style={a}>8(499) 252-01-32</Text>
          <Text style={h1}>АДРЕС</Text>
          <Text style={a}>м. 1905 года</Text>
          <Text style={a}>Шмитовский пр-д, 3с3</Text>
          <Text style={h1}>ПОЧТА</Text>
          <Text style={a}>info@cityretreat.ru</Text>
          <Text style={a}>БУДНИ c 8:00 - 23:00</Text>
          <Text style={a}>СУББОТА c 9:00 - 16:00</Text>
          <View style={{ flexDirection: 'row', paddingTop: 30, justifyContent: 'center' }}>
            <IconS 
              onPress={() => Linking.openURL('https://www.facebook.com/cityretreat.ru')}
              style={icon} name='social-facebook' size={30} color={constants.SECONDARY} />
            <IconS 
              onPress={() => Linking.openURL('https://www.instagram.com/cityretreat.ru')}
              style={icon} name='social-instagram' size={30} color={constants.SECONDARY} />
            <IconS 
              onPress={() => Linking.openURL('https://vk.com/aquarianageyogaclub')}
              style={icon} name='social-vkontakte' size={30} color={constants.SECONDARY} />
          </View>

          <View style={{ marginTop: 20 }}>
            <Map/>
          </View>
          <View style={{ margin: 10 }}>
            <Text style={b}>ООО "Сити Ретрит Клуб", ОГРН 5177746174667</Text>
            <Text style={b}>Юридический адрес: 123100, Москва, Шмитовский проезд, д.3, д.1, цоколь, пом. 4, комната 4</Text>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: constants.WHITE
  },
  video: {
  },
  content: {
    flex: 1,
    justifyContent: 'center'
  },
  h1: {
    fontFamily: constants.FONT,
    color: constants.FONTCOLOR,
    fontWeight: '300',
    fontSize: 17,
    paddingLeft: 20,
    paddingTop: 10
  },
  a: {
    fontFamily: constants.FONT,
    fontSize: 15,
    paddingLeft: 20,
    color: constants.LIGHT_GRAY,
    paddingTop: 5 
  },
  b: {
    fontFamily: constants.FONT,
    fontSize: 15,
    paddingTop: 10,
    color: constants.LIGHT_GRAY
  },
  map: {
    marginTop: 1,
    alignSelf: 'stretch',
    width: win.width,
    height: win.width 
  },
  icon: {
    paddingLeft: 15
  }
})


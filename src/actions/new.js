export const getDataNews = (item) => dispatch => {
  try {
    dispatch({ type: 'GET_DATA_NEWS', payload: item })
  } catch (error) {
    dispatch({ type: 'GET_DATA_NEWS_FAIL' })
  }
}

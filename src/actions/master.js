export const getDataMasters = (item) => dispatch => {
  try {
    dispatch({ type: 'GET_DATA_MASTERS', payload: item })
  } catch (error) {
    dispatch({ type: 'GET_DATA_MASTERS_FAIL' })
  }
}

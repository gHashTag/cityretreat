export const getDataPrices = (item) => dispatch => {
  try {
    dispatch({ type: 'GET_DATA_PRICES', payload: item })
  } catch (error) {
    dispatch({ type: 'GET_DATA_PRICES_FAIL' })
  }
}

export const getDataServices = (item) => dispatch => {
  //console.log('getDataServices', item)
  try {
    dispatch({ type: 'GET_DATA_SERVICES', payload: item })
  } catch (error) {
    dispatch({ type: 'GET_DATA_SERVICES_FAIL' })
  }
}

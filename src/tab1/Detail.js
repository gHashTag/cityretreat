import React, { Component } from 'react'
import {
  View,
  TextInput,
  ScrollView
} from 'react-native'
import { Fumi } from 'react-native-textinput-effects'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import FitImage from 'react-native-fit-image'
import { 
  Header
} from '../common'
import { constants } from '../constants'

class Detail extends Component {
  static navigationOptions = ({ navigation }) => ({
    header:  
      <Header 
        title={navigation.state.params.subtitle} 
        leftButton
        leftIcon='ios-arrow-back'
        colorLeft={constants.SECONDARY}
        rightButton
        rightIcon='md-create'
        colorRight={constants.PRIMARY}
        navigation={navigation} 
      />
  })

  render() {
    const { title, subtitle, img, info } = this.props.navigation.state.params
    const { container, inputContainer, inputStyle } = styles

    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={container}>
          <FitImage source={{ uri: img }} />
          <View style={{ paddingTop: 20, paddingBottom: 200 }}>
            <Fumi
              iconClass={FontAwesome}
              iconName={'list'}
              iconColor={constants.SECONDARY}
              iconSize={21}
              autoCorrect={false}
              returnKeyType='done'
              editable = {false}
              underlineColorAndroid='transparent'
              value={title}
              inputStyle={inputStyle}
              style={{ backgroundColor: 'transparent' }}
            />
            <Fumi
              iconClass={FontAwesome}
              iconName={'bullhorn'}
              iconColor={constants.SECONDARY}
              iconSize={21}
              autoCorrect={false}
              returnKeyType='done'
              autoCapitalize='words'
              editable = {false}
              underlineColorAndroid='transparent'
              value={subtitle}
              inputStyle={inputStyle}
              style={{ backgroundColor: 'transparent' }}
            />
            <TextInput
              style={inputContainer}
              placeholder="Подробности" 
              editable={false}
              multiline
              value={info}
            />
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: constants.WHITE,
    position: 'relative'
  },
  inputContainer: {
    fontSize: 19,
    width: '100%',
    color: constants.FONTCOLOR, 
    fontFamily: constants.FONT, 
    backgroundColor: 'transparent',
    padding: 15,
    fontWeight: '400' 
  },
  inputStyle: { 
    color: constants.FONTCOLOR, 
    fontFamily: constants.FONT, 
    backgroundColor: 'transparent',
    paddingBottom: 23,
    fontWeight: '400' 
  }
}

export default Detail

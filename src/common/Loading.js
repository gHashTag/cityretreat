import React, { Component } from 'react'
import {
  StyleSheet,
  View
} from 'react-native'

import Spinner from 'react-native-spinkit'

export default class Loading extends Component {
  constructor(props) {
    super(props)
    this.state = {
      index: 2,
      types: ['CircleFlip', 
        'Bounce', 
        'Wave', 
        'WanderingCubes', 
        'Pulse', 
        'ChasingDots', 
        'ThreeBounce', 
        'Circle', 
        '9CubeGrid', 
        'WordPress', 
        'FadingCircle', 
        'FadingCircleAlt', 
        'Arc', 
        'ArcAlt'],
      size: 50,
      color: 'gray',
      isVisible: true
    }
  }
  render() {
    const type = this.state.types[this.state.index]
    return (
      <View style={styles.container}>
        <Spinner style={styles.spinner} isVisible={this.state.isVisible} size={this.state.size} type={type} color={this.state.color}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 100
  }
})

export { Loading }

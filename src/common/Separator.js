import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'

const style = StyleSheet.create({
  main: {
    height: 2,
    width: '96%',
    backgroundColor: 'transparent',
    marginLeft: '2%'
  }
})

export default class Separator extends Component {
  render() {
    return <View style={style.main} />
  }
}

export { Separator }

import React, { Component } from 'react'
import { Dimensions, TouchableOpacity, StyleSheet, View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

export default TabHeader = ({ backButton, imageButton, title, navigation }) => {
  return (
    <View style={styles.container}>
      {backButton === true &&
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Icon name={'ios-arrow-back'} style={styles.backButton} />
        </TouchableOpacity>
      }
      <Text style={styles.title}>{title}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: 45,
    backgroundColor: '#242323',
    flexDirection: 'row',
    alignItems: 'center',
    position: 'relative'
  },
  title: {
    flex: 1,
    color: '#00FFC5',
    fontFamily: 'AppleSDGothicNeo-Light',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: 15,
  },
  backButton: {
    color: '#00FFC5',
    paddingTop: 5,
    paddingLeft: 10,
    fontSize: 30,
  }
})

export { TabHeader }

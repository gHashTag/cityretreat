import React, { Component } from 'react'
import { WebView } from 'react-native'
import { Header } from '../common'
import { constants } from '../constants'

class User extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: 
      <Header 
        title='Личный кабинет'
        leftButton
        leftIcon='ios-arrow-back'
        colorLeft={constants.SECONDARY}
        rightButton
        rightIcon='ios-contact-outline'
        colorRight='#242323'
        navigation={navigation} 
      />
  })

  render() {
    return (
      <WebView
        source={{ uri: 'https://widgets.healcode.com/sites/24045/client' }}         
      />
    )
  }
}

export { User }

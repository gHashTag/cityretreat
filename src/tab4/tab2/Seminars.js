const WEBVIEW_REF = 'WEBVIEW_REF'
import React, { Component } from 'react'
import {
  StyleSheet,
  ActivityIndicator,
  View,
  Platform,
  TouchableOpacity,
  WebView
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

class Seminars extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showText: false,
      canGoBack: false
    }
  }


  onBack() {
    this.refs[WEBVIEW_REF].goBack()
  }

  onNavigationStateChange(navState) {
    this.setState({
      canGoBack: navState.canGoBack
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topbar}>
          <TouchableOpacity
            disabled={!this.state.canGoBack}
            onPress={this.onBack.bind(this)}
          >
            <Icon name='ios-arrow-back' size={40} style={this.state.canGoBack ? styles.topbarBack : styles.topbarBackDisabled} />
          </TouchableOpacity>
        </View>
        <WebView
          ref={WEBVIEW_REF}
          style={{ flex: 1 }}
          source={ Platform.OS === 'ios' ? require('./seminars.html') : { uri: 'file:///android_asset/seminars.html' }}
          onNavigationStateChange={this.onNavigationStateChange.bind(this)}
          startInLoadingState
          renderLoading={() => (
            <ActivityIndicator
              style={{ paddingTop: 50 }}
              animating
              size="large"
              color="black"
            />
          )}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  topbar: {
    flexDirection: 'row',
    height: 40,
    justifyContent: 'space-between'
  },
  download: {
    marginRight: 125,
    paddingTop: 20, 
    color: 'gray'
  },
  topbarBack: {
    color: 'gray',
    marginTop: 8, 
    marginLeft: 15
  },
  topbarBackDisabled: {
    color: '#fff',
    marginLeft: 15
  }
})

export default Seminars 

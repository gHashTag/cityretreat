import React from 'react'
import { StyleSheet, Text, Platform } from 'react-native'
import { TabNavigator } from 'react-navigation'
import Tab1 from './tab1/Schedule'
import Tab2 from './tab2/Seminars'
import { constants } from '../constants'

export default TabNavigator({
  Tab1: {
    screen: Tab1,
    navigationOptions: () => ({
      header: null,
      tabBarLabel: ({ tintColor }) => (
        <Text style={[styles.label, { color: tintColor }]}>Классы</Text>
      )
    })
  },
  Tab2: {
    screen: Tab2,
    navigationOptions: () => ({
      header: null,
      tabBarLabel: ({ tintColor }) => (
        <Text style={[styles.label, { color: tintColor }]}>Семинары</Text>
      )
    })
  }
}, {
  tabBarPosition: 'top',
  animationEnabled: false,
  swipeEnabled: false,
  lazyLoad: true,
  tabBarOptions: {
    indicatorStyle: { backgroundColor: constants.SECONDARY },
    activeTintColor: constants.SECONDARY,
    upperCaseLabel: false,
    labelStyle: {
      fontFamily: constants.FONT,
      fontSize: 12,
      margin: 35,
      justifyContent: 'center',
      alignItems: 'center',
      color: 'gold'
    },
    style: { 
      backgroundColor: constants.PRIMARY,
      height: Platform.OS === 'ios' ? 70 : 50 
    }
  }
})

const styles = StyleSheet.create({
  label: {
    fontFamily: 'AppleSDGothicNeo-Light',
    fontSize: 17,
    fontWeight: '500',
    textAlign: 'center',
    marginBottom: 15
  }
})

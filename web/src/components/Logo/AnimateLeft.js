import React, { PureComponent } from 'react'
import Plx from 'react-plx'
import { Image } from '../common'

class AnimateLeft extends PureComponent {
  render() {
    const { path, alt, start, duration } = this.props
    return (
      <Plx parallaxData={
        [{
          start,
          duration,
          properties: [
            {
              startValue: 0,
              endValue: -600,
              property: 'translateX'
            },
            {
              startValue: 1,
              endValue: 0,
              property: 'opacity'
            }
          ]
        }]
      } 
      style={{
        position: 'fixed',
        left: '50%',
        top: '17%'
      }}
      >
        <Image src={path} alt={alt} />
      </Plx> 	
    )
  }
}

export default AnimateLeft

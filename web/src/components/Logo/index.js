import React, { PureComponent } from 'react'
import { CityRetreat, MansionWellness, Stairs, Foundation, Penthhouse, Roof, LeftSide, RightSide, Column1, Column2, Column3, Column4, Column5, Column6, Window1, Window2, Window3, Window4, WindowUp1, WindowUp2, WindowUp3, WindowUp4, WindowUp5, Door } from './image' // eslint-disable-line
import AnimateUp from './AnimateUp'
import AnimateLeft from './AnimateLeft'
import AnimateRight from './AnimateRight'
//import AnimateDown from './AnimateDown'

class Logo extends PureComponent {
  render() {
    return (
      <div style={{ height: 1400 }}>
        <AnimateUp path={Penthhouse} start={0} duration={300} alt="CityRetreat" />
        <AnimateUp path={Roof} start={0} duration={600} alt="CityRetreat" />
        <AnimateUp path={WindowUp1} start={50} duration={600} alt="CityRetreat" />
        <AnimateUp path={WindowUp2} start={50} duration={700} alt="CityRetreat" />
        <AnimateUp path={WindowUp3} start={50} duration={800} alt="CityRetreat" />
        <AnimateUp path={WindowUp4} start={50} duration={900} alt="CityRetreat" />
        <AnimateUp path={WindowUp5} start={50} duration={1000} alt="CityRetreat" />
        <AnimateUp path={Column5} start={50} duration={600} alt="CityRetreat" />
        <AnimateUp path={Column4} start={50} duration={700} alt="CityRetreat" />
        <AnimateUp path={Column3} start={50} duration={800} alt="CityRetreat" />
        <AnimateUp path={Column2} start={50} duration={900} alt="CityRetreat" />
        <AnimateUp path={Window2} start={200} duration={600} alt="CityRetreat" />
        <AnimateUp path={Window3} start={200} duration={700} alt="CityRetreat" />
        <AnimateUp path={Window4} start={200} duration={800} alt="CityRetreat" />
        <AnimateLeft path={LeftSide} start={0} duration={300} alt="CityRetreat" />
        <AnimateLeft path={Column1} start={0} duration={600} alt="CityRetreat" />
        <AnimateRight path={Column6} start={0} duration={600} alt="CityRetreat" />
        <AnimateLeft path={Window1} start={50} duration={900} alt="CityRetreat" />
        <AnimateRight path={Door} start={50} duration={900} alt="CityRetreat" />
        <AnimateRight path={RightSide} start={0} duration={300} alt="CityRetreat" />
        <AnimateUp path={Foundation} start={390} duration={600} alt="CityRetreat" />
        <AnimateUp path={Stairs} start={390} duration={600} alt="CityRetreat" />
        <AnimateUp path={CityRetreat} start={400} duration={650} alt="CityRetreat" />
        <AnimateUp path={MansionWellness} start={400} duration={700} alt="CityRetreat" />
      </div>
    )
  }
}

export default Logo 

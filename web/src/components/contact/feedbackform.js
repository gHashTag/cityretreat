import React, { Component } from 'react'
import Form from 'muicss/lib/react/form'
import Input from 'muicss/lib/react/input'
import Textarea from 'muicss/lib/react/textarea'
import Button from 'muicss/lib/react/button'

export default class Feedback extends Component {
  render() {
    const { container } = styles
    return (
      <Form style={container}>
        <legend>Контакт</legend>
        <Input hint="Имя" />
        <Input hint="Email" />
        <Textarea hint="Сообщение" />
        <Button variant="raised">Отправить</Button>
      </Form>
    )
  }
}
const styles = {
  container: {
    padding: 30
  }
}

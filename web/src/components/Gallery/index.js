import React from 'react'
import Player from '../Backgroundvideo/Player'
import Gallery from './Gallery'

export default () => (
  <div>
    <Player />
    <Gallery />
  </div>
) 

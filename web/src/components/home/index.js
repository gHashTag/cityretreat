import React from 'react'
import { View, Text } from 'react-native' // eslint-disable-line
import Logo from '../Logo'
import Player from '../Backgroundvideo/Player'
import Events from '../Events'
import MouseScroller from '../MouseScroller'
import We from '../we'

export default () => (
  <div>
    <Player />
    <We />
    <MouseScroller />
    <Logo />
    <Events />
  </div>
)

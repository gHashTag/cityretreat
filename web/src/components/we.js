import React, { PureComponent } from 'react'
import { Platform, TouchableHighlight, Linking, PixelRatio, View, Image, Text, Dimensions } from 'react-native' // eslint-disable-line
import WebFont from 'webfontloader'
import AOS from 'aos'
import './aos.css'
import '../fonts.css'
 
const win = Dimensions.get('window')

const WebFontConfig = {
  custom: {
    families: ['Museo500', 'CirceLight', 'CirceExtraLight']
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    top: win.height * 1.8,
    alignItems: 'center'
  },
  textStyle1: {
    fontFamily: 'CirceExtraLight',
    fontSize: 120 / win.scale, 
    fontStyle: 'normal',
    lineHeight: 'normal',
    letterSpacing: 10.3,
    textAlign: 'center',
    color: '#fff'
  },
  line: {
    width: '15%',
    height: 3,
    backgroundColor: '#fff',
    opacity: 0.73
  },
  textStyle2: {
    width: (win.width / 2) + 68,
    height: 246,
    opacity: 0.73,
    fontFamily: 'CirceLight',
    fontSize: PixelRatio.getPixelSizeForLayoutSize(16) / win.scale,
    fontStyle: 'normal',
    lineHeight: 'normal',
    textAlign: 'center',
    color: '#fff'
  }
}

WebFont.load(WebFontConfig)
AOS.init()

export default class We extends PureComponent {
  render() {
    const { container, textStyle1, line, textStyle2 } = styles
    return (
      <View style={container}>
        <Text 
          data-aos="fade-up"
          data-aos-duration="2000" 
          data-aos-delay="200" 
          style={textStyle1}
        >О НАС</Text>
        <View 
          data-aos="fade-up"
          data-aos-duration="2000" 
          data-aos-delay="100" 
          style={line} 
        />
        <Text 
          data-aos="fade-up"
          data-aos-duration="3000" 
          data-aos-delay="100" 
          className="museo"
          style={textStyle2}
        >
        Особняк в центре Москвы, где вы можете насладиться здоровым образом жизни, расслабиться и восстановить силы.
Уникальный формат Центра – сочетание современных высокотехнологичных систем лечения с древними традициями оздоровительных практик. Обучение методикам омоложения, принципам осознанного отношения к здоровью, питанию, избавлению от зависимостей.
        </Text>
      </View>
    )
  }
}

import React, { PureComponent } from 'react'
import { View, Image, PixelRatio, Dimensions } from 'react-native'

const win = Dimensions.get('window')
const response = PixelRatio.getPixelSizeForLayoutSize

export default class MenuName extends PureComponent {
  render() {
    const { tx, ty, source } = this.props
    return (
      <View style={{
        flex: 1,
        position: 'fixed',
        zIndex: 5,
      }}
      >
        <Image 
          style={{
            //top: (win.height * win.scale) / (response(2) - 295), 
            //left: (win.width / 2) - (response(150) / win.scale),
            //width: response(300) / win.scale, 
            //height: response(233) / win.scale,
            //paddingLeft: -win.width/2, 
            //height: win.height,
            //width: win.height/2.68,
            //transform: [
              //{ translateX: tx },
              //{ translateY: ty }
            //]
          }} 
          source={source}
        />
      </View>
    )
  }
}

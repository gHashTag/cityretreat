import React, { Component } from 'react'
import { View, Text } from 'react-native' // eslint-disable-line
import BurgerMenu from 'react-burger-menu'
import MenuWrap from './MenuWrap'
import './App.css'
import Home from './components/Home'
import Events from './components/Events'
import Gallery from './components/Gallery'
import Construction from './components/Construction'
import Masters from './components/Masters'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      route: window.location.hash.substr(1),
      currentMenu: 'stack',
      isOpen: false,
      side: 'left'
    }
  }

  componentDidMount() {
    window.addEventListener('hashchange', () => {
      this.setState({
        route: window.location.hash.substr(1)
      })
    })
  }

  getItems() {
    const items = [
      <a key='0' href='#/home'><i className='fa fa-fw fa-university' /><span>Главная</span></a>,
      <a key='1' href='#/events'><i className='fa fa-fw fa-star-o' /><span>События</span></a>,
      <a key='2' href='#/gallery'><i className='fa fa-fw fa-image' /><span>Галерея</span></a>,
      <a key='3' href='#/masters'><i className='fa fa-fw fa-graduation-cap' /><span>Мастера</span></a>,
      //<a key='3' href='/timetable'><i className='fa fa-fw fa-calendar' /><span>Расписание</span></a>,
      //<a key='5' href='/restaurant'><i className='fa fa-fw fa-leaf' /><span>Ресторан</span></a>,
      //<a key='7' href='/contact'><i className='fa fa-fw fa-globe' /><span>Контакт</span></a>
    ]
    return items
  }

  changeMenu(menu) {
    this.setState({ currentMenu: menu })
  }

  changeSide(side) {
    this.setState({ side })
  }

  render() {
    let Child
    const Menu = BurgerMenu[this.state.currentMenu]
    const items = this.getItems()

    switch (this.state.route) {
    case '/home': Child = Home 
      break
    case '/events': Child = Events 
      break
    case '/gallery': Child = Gallery 
      break
    case '/masters': Child = Masters 
      break
    case '/timetable': Child = Construction 
      break
    case '/restaurant': Child = Construction 
      break
    case '/shop': Child = Construction 
      break
    case '/contact': Child = Construction 
      break
    default: Child = Home 
    }

    return (
      <div>
        <MenuWrap wait={20} side={this.state.side}>
          <Menu isOpen={this.state.isOpen} id={this.state.currentMenu} pageWrapId={'page-wrap'} outerContainerId={'outer-container'} left>
            {items}
          </Menu>
        </MenuWrap>
        <Child />
      </div>
    )
  }
}

export default App
